Dissolution of the Governance Body per Article V of the Charter
===============================================================

On  May 31, 2019 the board of Onyx Point, Inc. voted to dissolve the SIMP
Council.

This decision was not made lightly and was made to help accelerate the overall
focus and development of the SIMP project. While the Council was formed with
all of the best intentions, it was unfortunately far too large of a structure
to pursue with a small team while attempting to maintain product acceleration
velocity.

At this time, direction and control of the SIMP Community Edition project is
being held by Trevor Vaughan of Onyx Point, Inc. and all community input is
VERY welcome!

We highly encourage users and community members to get in touch via one of the
methods available via http://simp-project.com.
