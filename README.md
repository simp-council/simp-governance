SIMP Project Governance Archive
===============================

---
> This Repository Exists for Historic Reference Only
>
> The project and community continues on at http://simp-project.com
---

This repository contains all official SIMP Project documents.

 * [Dissolution of the Governance Body per Article V of the Charter](dissolution.md)
 * [Project Charter](charter.md)
 * [Project Bylaws](bylaws.md)
 * [Community Code of Conduct](code_of_conduct.md)
 * [Committee Charters](committees/README.md)

---

Copyright © 2017, Onyx Point, Inc.</br>
All content licensed under the [Creative Commons Attribution 4.0 International License](LICENSE.md) unless otherwise noted.
