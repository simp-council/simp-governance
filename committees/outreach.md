# The SIMP Outreach Committee Charter

## Charge

The SIMP Outreach Committee is responsible for advising the SIMP Council on issues related to communication with and within the SIMP Community.
Further, the SIMP Outreach Committee is tasked with engaging community members via both official and unofficial communications channels.

## Powers & Responsibilities

The SIMP Outreach Committee is delegated the authority of the SIMP Council to make decisions and policy with regard to the following:

  * Official publications
  * Official communications channels
  * Design and maintenance of the SIMP visual brand

Decisions which may have an impact beyond these areas, or which require long-term planning (one year or longer), must be referred to the SIMP Council.
The interpretation of such is left to the SIMP Outreach Committee, however, the SIMP Council reserves the right to review any and all decisions of the SIMP Outreach Committee.

## Meetings

The SIMP Outreach Committee shall meet no less than once per calendar month, or when necessary at the call of the committee chair.
Meeting minutes shall be posted to the *simp-dev* mailing list within one week of each meeting to record the decisions of the SIMP Outreach Committee.

## Members

 * Amanda Arnold (Chair)
 * Dylan Cochran
 * Nick Miller
 * Ryan Russell-Yates
 * Jay Stoner
 * Lisa Umberger
