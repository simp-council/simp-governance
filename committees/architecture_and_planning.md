# The SIMP Architecture & Planning Committee

## Charge

The SIMP Architecture & Planning Committee ("APC") is responsible for advising the SIMP Council on issues related to software architecture.
Additionally, the APC is tasked with software architecture planning over the short and medium term (less than one year).

## Powers & Responsibilities

The APC is delegated the authority of the SIMP Council to make decisions and policy in the following domains:

 * SIMP software architecture
 * Goals and expectations for the start- and end-states of users' systems
 * Software engineering priorities

Decisions which may have an impact beyond these areas, or which require long-term planning (one year or longer), must be referred to the SIMP Council.
The interpretation of such is left to the APC, however, the SIMP Council reserves the right to review any and all decisions of the APC.

## Meetings

The APC shall meet no less than once per calendar month, or when necessary at the call of the committee chair.
Meeting minutes shall be posted to the *simp-dev* mailing list within one week of each meeting to record the decisions of the APC.

## Members

 * Lucas Yamanishi (Chair)
 * Dylan Cochran
 * Nick Miller
 * Kendall Moore
 * Chris Tessmer
 * Trevor Vaughan
