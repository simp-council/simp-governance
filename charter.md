# Charter

## Preamble

Being the primary developers and current stewards of The System Integrity Management Platform ("The SIMP" or "SIMP"), Onyx Point, Incorporated ("Onyx Point"), a corporation of the State of Maryland, does hereby ordain and establish this Project.

## Definitions

**_"Community"_** means the collective body of current users, developers, promoters, and others contributing to SIMP.

**_"Communal Assets"_** means all assets made at least partially available to the entire SIMP Community, either publicly or on request.  This should not be construed as providing any guarantee of right to these assets.

**_"Open Source Software"_** means Software licensed under the terms of a license approved by the Open Source Initiative, or otherwise approved by the SIMP Council.

## Article I: Name of the Project

The name of the Project is "The System Integrity Management Platform Project," abbreviated as "SIMP Project."

## Article II: Purpose

The purpose of the Project is to manage the SIMP Open Source Software.  This includes, but is not limited to, defining SIMP, coordinating architecture and release decisions, managing Communal Assets, and otherwise organizing the SIMP Community to achieve its collective goals.

## Article III: SIMP Council

All Project powers shall be executed by or under the authority of the SIMP Council.  The number of Councilmembers shall be five (5) which number may be increased or decreased pursuant to the bylaws of the Project ("Bylaws"), not to exceed fifteen (15).  Only SIMP Project Members are eligible for SIMP Council office.  Onyx Point may, at its sole discretion, at any time, appoint up to one half of the Councilmembers, plus one (1) additional Councilmember; otherwise, Councilmembers will be elected by a vote of Project Members pursuant to the Bylaws.  Each of the Councilmembers may hold office for a term of one (1) year.  Councilmembers may hold office for multiple terms if duly elected in accordance with the Bylaws, or, if appointed by Onyx Point, with the advice and consent of the SIMP Council.  The names of the Councilmembers who shall act for the first term or until new Councilmembers are duly chosen are as follows:

 - Judith Johnson
 - Kendall Moore
 - Lisa Umberger
 - Trevor Vaughan
 - Lucas Yamanishi

## Article IV: Membership

Membership will be determined on a case-by-case basis at the sole discretion of the SIMP Council.  Any contributor to SIMP shall be eligible for membership.  A "contributor" shall be defined as any natural person who has contributed to a non-trivial improvement of the SIMP Project, such as code, documentation, translation, maintenance of Project resources, graphic design, substantial promotion, community organization, or other non-trivial activities which benefit the SIMP Project.

## Article V: Organization & Dissolution

The SIMP Project is sponsored by Onyx Point who remains liable for the actions of the Project.  As such, Onyx Point retains full, exclusive right to the Project and may at any time, for any purpose, dissolve the Project.
